CC = gcc -Wall -Werror -Wextra	

SRCS =	./srcs/ft_printf.c		\
		./srcs/check.c			\
		./srcs/utils.c			\
		./srcs/transfo.c		\
		./libpf/pf_atoi.c		\
		./libpf/pf_calloc.c		\
        ./libpf/pf_isdigit.c	\
		./libpf/pf_itoa_addr.c	\
		./libpf/pf_itoa_base.c	\
		./libpf/pf_itoa.c		\
        ./libpf/pf_memset.c		\
        ./libpf/pf_putchar_fd.c	\
    	./libpf/pf_putstr_fd.c	\
        ./libpf/pf_strchr.c		\
        ./libpf/pf_strdup.c		\
		./libpf/pf_strjoin.c	\
        ./libpf/pf_strlen.c		\
		./libpf/pf_strnew.c		\
		./libpf/pf_substr.c	

OBJS	=	${SRCS:.c=.o}
HEADERS	= ./includes
RM		= rm -f
NAME	= libftprintf.a
all:	${NAME}
$(NAME):	${OBJS}
			ar -rc ${NAME} $^
			ranlib ${NAME}
%.o:%.c
	${CC} -o $@ -c $<
clean	:
			${RM} ${OBJS}
fclean	: clean
			${RM} ${NAME}
re		: fclean all
