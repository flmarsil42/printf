# Printf
Résumé: Ce projet est clair et efficace. Vous devez recoder printf. Vous aurez dès lors la possibilité de le réutiliser dans vos futurs projets. Ce projet porte principalement sur les arguments à taille variable.

Skills:
- Rigor
- Algorithms & AI 

Lire le [sujet][1]

`Ce projet a été codé sur Macos`

### Compiler et lancer le projet

1. Téléchargez / Clonez le dépot

```
git clone https://gitlab.com/flmarsil42/ft_printf && cd ft_printf
```

2. Compilez le projet et utilisez le fichier libftprintf.a avec votre main

```
Make
gcc -Wall -Werror -Wextra main.c libftprintf.a
```

[1]:https://gitlab.com/flmarsil42/printf/-/blob/master/fr.subject.pdf
